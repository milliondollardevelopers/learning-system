Param (
	[Parameter(Mandatory=$True)]
	[string]$AzureResourceGroup,
	[Parameter(Mandatory=$True)]
        [string]$SwarmManagerVM,
	[Parameter(Mandatory=$True)]
        [string]$Dest_Loc,
        [Parameter(Mandatory=$True)]
        [string]$cdt_sas_uri,
        [Parameter(Mandatory=$True)]
        [string]$ScriptPath
)


$PSVersionTable
$release_id  = $OctopusParameters["Octopus.Release.Number"]

Write-Output "AzureResourceGroup is $AzureResourceGroup"
Write-Output "SwarmManagerVM is $SwarmManagerVM"
Write-Output "SwarmDeployScript $ScriptPath"
Write-Output "Release version is $release_id"
Write-Output "SAS URI is $cdt_sas_uri"
Write-Output "Destination Loc is $Dest_Loc"


#Invoke-AzureRmVMRunCommand -ResourceGroupName $AzureResourceGroup -Name $SwarmManagerVM -CommandId 'RunShellScript' -ScriptPath "$ScriptPath" -Parameter @{"arg1" = "$Dest_Loc";"arg2" = "$cdt_sas_uri";"arg3" = "$release_id"}

Invoke-AzureRmVMRunCommand -ResourceGroupName $AzureResourceGroup -Name $SwarmManagerVM -CommandId 'RunShellScript' -ScriptPath "$ScriptPath" -Parameter @{"arg1" = "$Dest_Loc" ; "arg2" = "$release_id"}
