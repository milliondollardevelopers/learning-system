

data = {"propertyLabel": ["156MCHWVlvPsn.PV", "156MClgCall.PV", "156MDuctPrsSp.PV", "156MEaDmpPsn.PV"], \
"point_names": ["156MCHWVlvPsn", "156MClgCall", "156MDuctPrsSp", "156MEaDmpPsn"], \
"extension": ["PV", "PV", "PV", "PV"], \
"propertyComment": ["L56 ACU 1 CHW Valve Position", "L56 ACU 1 Cooling Call", "L56 ACU 1 Duct Pressure SP", "L56 ACU1 Exhaust Damper Psn"], \
"model": ["Control Point Function Type", "Control Point Function Type", "Control Point Function Type", "Control Point Function Type"], \
"NewAspectValue": ["Command", "Present Value", "Setpoint", "Command"], \
"Building": ["rialto", "rialto", "rialto", "rialto"], \
"Label": ["2", "3", "4", "2"] }