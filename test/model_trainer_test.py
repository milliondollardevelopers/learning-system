from unittest import TestCase
import pandas as pd
from time import gmtime, strftime
from test.mock_data import data

from engine_util.training.model_training import ModelTrainer

class TestModelTrainer(TestCase):

    def test_instantiation(self):
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        with self.assertRaises(TypeError):
            ModelTrainer(df="df", start_time=start_time)

    def test_instantiation2(self):
        df = pd.DataFrame(data)
        with self.assertRaises(ValueError):
            ModelTrainer(df=df, start_time="2:03")

    def test_instantiation3(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        with self.assertRaises(TypeError):
            ModelTrainer(df=df, start_time=start_time, model_type=0)

    def test_instantiation4(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        with self.assertRaises(TypeError):
            ModelTrainer(df=df, start_time=start_time, epoch_num="100")

    def test_instantiation5(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        with self.assertRaises(ValueError):
            ModelTrainer(df=df, start_time=start_time, epoch_num=0)

    def test_instantiation6(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        with self.assertRaises(ValueError):
            ModelTrainer(df=df, start_time=start_time, epoch_num=-5)

    def test_instantiation7(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        with self.assertRaises(TypeError):
            ModelTrainer(df=df, start_time=start_time, batch_size="100")

    def test_instantiation8(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        with self.assertRaises(ValueError):
            ModelTrainer(df=df, start_time=start_time, batch_size=0)

    def test_instantiation9(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        with self.assertRaises(ValueError):
            ModelTrainer(df=df, start_time=start_time, batch_size=-10)

    def test_instantiation10(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        with self.assertRaises(TypeError):
            ModelTrainer(df=df, start_time=start_time, test_size=".02")

    def test_instantiation11(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        with self.assertRaises(TypeError):
            ModelTrainer(df=df, start_time=start_time, test_size=1)

    def test_instantiation12(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        with self.assertRaises(ValueError):
            ModelTrainer(df=df, start_time=start_time, test_size=0.0)

    def test_instantiation13(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        with self.assertRaises(ValueError):
            ModelTrainer(df=df, start_time=start_time, test_size=-0.02)

    def test_instantiation14(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        with self.assertRaises(ValueError):
            ModelTrainer(df=df, start_time=start_time, test_size=1.0)

    def test_instantiation15(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        with self.assertRaises(ValueError):
            ModelTrainer(df=df, start_time=start_time, test_size=1.1)

    def test_instantiation16(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        with self.assertRaises(TypeError):
            ModelTrainer(df=df, start_time=start_time, learning_rate=1)

    def test_instantiation17(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        with self.assertRaises(TypeError):
            ModelTrainer(df=df, start_time=start_time, learning_rate="0.003")

    def test_instantiation18(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        with self.assertRaises(ValueError):
            ModelTrainer(df=df, start_time=start_time, learning_rate=-0.01)

    def test_instantiation19(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        with self.assertRaises(ValueError):
            ModelTrainer(df=df, start_time=start_time, learning_rate=0.0)

    def test_preprocess_data(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        model_trainer = ModelTrainer(df=df, start_time=start_time)
        model_trainer.preprocess_data()
        test_df = pd.read_csv("./test/test_mock_data.csv")
        assert(test_df.iat[52, 6] == model_trainer._df.iat[52,6] == 20)

    def test_preprocess_data2(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        model_trainer = ModelTrainer(df=df, start_time=start_time)
        model_trainer.preprocess_data()
        test_df = pd.read_csv("./test/test_mock_data.csv")
        assert(test_df.iat[3452, 59] == model_trainer._df.iat[3452,59] == 1)


    """ Test create_train_test """
    def test_preprocess_data3(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        model_trainer = ModelTrainer(df=df, start_time=start_time)
        model_trainer.preprocess_data()

        x_train = len(model_trainer._train_x)
        y_train = len(model_trainer._train_y)
        x_test = len(model_trainer._test_x)
        y_test = len(model_trainer._test_y)

        assert(x_train != 0 and y_train != 0 and x_test != 0 and y_test != 0)

    """ Test create_train_test """
    def test_preprocess_data4(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        model_trainer = ModelTrainer(df=df, start_time=start_time, test_size=.5)
        model_trainer.preprocess_data()

        x_train = len(model_trainer._train_x)
        y_train = len(model_trainer._train_y)
        x_test = len(model_trainer._test_x)
        y_test = len(model_trainer._test_y)

        assert(x_train == y_train == x_test == y_test == 100894)

    def test_preprocess_data5(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        model_trainer = ModelTrainer(df=df, start_time=start_time, test_size=.25)
        model_trainer.preprocess_data()

        x_train = len(model_trainer._train_x)
        y_train = len(model_trainer._train_y)
        x_test = len(model_trainer._test_x)
        y_test = len(model_trainer._test_y)

        assert(x_train == y_train == 151341 and x_test == y_test == 50447)

    def test_train(self):
        pass

    """ Test create_model """
    def test_train2(self):
        pass
    
    def test_test_lol(self):
        pass

    def test_calculate(self):
        pass

    """ Tests 0 test case """
    def test_set_epoch_num(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        model_trainer = ModelTrainer(df=df, start_time=start_time)
        model_trainer.set_epoch_num(0)
        assert(model_trainer._epoch_num == 500)

    """ Tests negative number test case """
    def test_set_epoch_num2(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        model_trainer = ModelTrainer(df=df, start_time=start_time, epoch_num=1000)
        model_trainer.set_epoch_num(-1)
        assert(model_trainer._epoch_num == 1000)

    """ Tests if value gets set in the right attribute field correctly """
    def test_set_epoch_num3(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        model_trainer = ModelTrainer(df=df, start_time=start_time, epoch_num=200)
        model_trainer.set_epoch_num(100)
        assert(model_trainer._epoch_num == 100)

    """ Tests if TypeError gets raised with string input """
    def test_set_epoch_num4(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        model_trainer = ModelTrainer(df=df, start_time=start_time, epoch_num=200)
        model_trainer.set_epoch_num("100")
        assert(model_trainer._epoch_num == 200)

    """ Tests if TypeError gets caught when input isn't int """
    def test_set_epoch_num5(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        model_trainer = ModelTrainer(df=df, start_time=start_time)
        model_trainer.set_epoch_num(100.6)
        assert(model_trainer._epoch_num == 500)

    """ Tests 0 test case """
    def test_set_batch_size(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        model_trainer = ModelTrainer(df=df, start_time=start_time)
        model_trainer.set_batch_size(0)
        assert(model_trainer._batch_size == 100)

    """ Tests negative number test case """
    def test_set_batch_size2(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        model_trainer = ModelTrainer(df=df, start_time=start_time, batch_size=50)
        model_trainer.set_batch_size(-5)
        assert(model_trainer._batch_size == 50)

    """ Tests if value gets set in the right attribute field correctly """
    def test_set_batch_size3(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        model_trainer = ModelTrainer(df=df, start_time=start_time, batch_size=100)
        model_trainer.set_batch_size(40)
        assert(model_trainer._batch_size == 40)

    """ Tests if inputting string is okay """
    def test_set_batch_size4(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        model_trainer = ModelTrainer(df=df, start_time=start_time, batch_size=100)
        model_trainer.set_batch_size("40")
        assert(model_trainer._batch_size == 100)

    """ Tests if TypeError gets caught when input can't be directly cast as int"""
    def test_set_batch_size5(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        model_trainer = ModelTrainer(df=df, start_time=start_time)
        model_trainer.set_batch_size(40.5)
        assert(model_trainer._batch_size == 100)

    def test_set_test_size(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        model_trainer = ModelTrainer(df=df, start_time=start_time)
        model_trainer.set_test_size(0.0)
        assert(model_trainer._test_size == 0.2)

    def test_set_test_size2(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        model_trainer = ModelTrainer(df=df, start_time=start_time, test_size=0.3)
        model_trainer.set_test_size(-10.0)
        assert(model_trainer._test_size == 0.3)

    def test_set_test_size3(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        model_trainer = ModelTrainer(df=df, start_time=start_time)
        model_trainer.set_test_size(1.0)
        assert(model_trainer._test_size == 0.2)

    def test_set_test_size4(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        model_trainer = ModelTrainer(df=df, start_time=start_time)
        model_trainer.set_test_size(1.2)
        assert(model_trainer._test_size == 0.2)

    def test_set_test_size5(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        model_trainer = ModelTrainer(df=df, start_time=start_time, test_size=0.2)
        model_trainer.set_test_size(0.3)
        assert(model_trainer._test_size == 0.3)

    def test_set_test_size6(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        model_trainer = ModelTrainer(df=df, start_time=start_time, test_size=0.2)
        model_trainer.set_test_size("0.3")
        assert(model_trainer._test_size == 0.2)

    def test_set_learning_rate(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        model_trainer = ModelTrainer(df=df, start_time=start_time)
        model_trainer.set_learning_rate(0.003)
        assert(model_trainer._learning_rate == 0.003)

    def test_set_learning_rate2(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        model_trainer = ModelTrainer(df=df, start_time=start_time)
        model_trainer.set_learning_rate(0)
        assert(model_trainer._learning_rate == 0.0001)

    def test_set_learning_rate3(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        model_trainer = ModelTrainer(df=df, start_time=start_time, learning_rate=0.0005)
        model_trainer.set_learning_rate(0.0)
        assert(model_trainer._learning_rate == 0.0005)

    def test_set_learning_rate4(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        model_trainer = ModelTrainer(df=df, start_time=start_time, learning_rate=0.002)
        model_trainer.set_learning_rate(-0.7)
        assert(model_trainer._learning_rate == 0.002)

    def test_set_learning_rate5(self):
        df = pd.DataFrame(data)
        start_time = strftime("%Y%m%d%H%M%S", gmtime())
        model_trainer = ModelTrainer(df=df, start_time=start_time)
        model_trainer.set_learning_rate(0.1)
        assert(model_trainer._learning_rate == 0.1)

    
