from unittest import TestCase
import pandas as pd

from engine_util.training.nc_model_helper import NamingConventionModelHelper


class TestModelHelper(TestCase):
    def test_transform_point(self):
        model_helper = NamingConventionModelHelper()
        point_name = "VAV_1_22_10_SpaceTemp"
        transformed_point = model_helper.transform_point(point_name)
        transformed_array = [48, 27, 48, 53, 56, 53, 57, 57, 53, 56, 55, 53, 45, 16, 1, 3, 5, 46, 5, 13, 16, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        assert(transformed_array==transformed_point)

    def test_point_list_transformation(self):
        model_helper = NamingConventionModelHelper()
        row_data  = ["VAV_1_22_10_SpaceTemp","VAV_1_22_20_HtgStg2"]
        transformed_point_array = model_helper.point_list_converter(row_data)
        print(transformed_point_array)
        transformed_point_array_actual = [[48, 27, 48, 53, 56, 53, 57, 57, 53, 56, 55, 53, 45, 16, 1, 3, 5, 46, 5, 13, 16, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [48, 27, 48, 53, 56, 53, 57, 57, 53, 57, 55, 53, 34, 20, 7, 45, 20, 7, 57, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]
        assert(transformed_point_array_actual == transformed_point_array)
