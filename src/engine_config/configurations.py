import os

fuseki_ground_truth_url="http://131.201.163.182:8080"
skyspark_ground_truth_url="http://localhost:5001/api/tags"
sql_ip = 'localhost'
sql_username = 'root'
sql_password = os.getenv("$SQL_PWD")
sql_db = 'metrics_db'
sql_port = '3306'
table_name = 'obs_model_metrics_v11'
site_list = ['bcg_york','bcg_curiest','bcg_nnswhealthba','bcg_burjkhalifa','bcg_nnswhealthca','bcg_crownpodium','bcg_darwincec','bcg_emmarsouthridge',
        'bcg_etobicoke','bcg_goldenvalley','bcg_madridoffice','bcg_ladycilento','bcg_longbayforensic','bcg_longbayprison','bcg_phoenixmaryvale',
        'bcg_mlccentre','bcg_morrisplains','bcg_mpha','bcg_nnswhealthgr','bcg_nnswhealthmu','bcg_princesstower','bcg_qcc','bcg_rialto',
        'bcg_richardsonplace','bcg_rockandrollhof','bcg_stmildred','bcg_stardarling','bcg_phoenixtrevorbrowne','bcg_trillium','bcg_victoriancourts','bcg_wembley']
tenant_authorization_ep = "https://login.windows.net/f0269474-ac95-4f5b-95a9-f7a8abe779b3/oauth2/token"
resource = 'http://cbpt01atqaweb.azurewebsites.net'
client_id = ''
client_secret = ""
tenant_name = 't01atqacloudapp'
sas_uri=os.getenv("$SAS_URI")