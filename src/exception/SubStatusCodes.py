from enum import Enum

class ResponseSubStatusCodes(Enum):
    PP_4002="Invalid InputParameters"
    PP_4003="Input request has insufficient data"
    PP_4004="Invalid PredictionLevel"
    PP_4005="PredictionLevel Mismatch"
    PP_5001="Error in Model Validation"
    PP_5002="Error in Model Transformation"
    PP_5003="Error in Model Prediction"
    PP_5004="Invalid Model"
    PP_5005="General Error"
    PP_5006="Invalid Model Mapping"
    PP_5007="Invalid Model Prediction Response JSON Structure"