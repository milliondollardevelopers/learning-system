import string
import numpy as np
from keras.layers import Activation
from keras.layers import Dense
from keras.models import Sequential
from keras.models import model_from_json
from keras.optimizers import SGD
import pandas as pd
from sklearn.model_selection import train_test_split


class NamingConventionModelHelper:


    """Function to transform array of points to form suitable for tensor inputs"""

    def point_list_converter(self, point_list):
        transformed_point_list = []
        for point in point_list:
            transformed_point = NamingConventionModelHelper.transform_point(self, point)
            transformed_point_list.append(transformed_point)
        return transformed_point_list

    """Function to transform characters to digits for consumption by NN"""

    def transform_point(self, point):
        values = dict()
        for index, letter in enumerate(string.ascii_lowercase):
            values[letter] = index + 1
        """ uppercase from 27 to 52 """
        values2 = dict(values)
        for index, letter in enumerate(string.ascii_uppercase):
            values2[letter] = index + 27
        """ underscore to 53 """
        letter1 = "_"
        values2[letter1] = 53
        letter2 = "-"
        values2[letter2] = 54
        """ add to dictionary"""
        letter3 = "%"
        values2[letter3] = 65
        letter4 = ''
        values2[letter4] = 66
        """ numbers 0 to 9 """
        numlist = {'0': 55, '1': 56, '2': 57, '3': 58, '4': 59,
                   '5': 60, '6': 61, '7': 62, '8': 63, '9': 64}
        values2.update(numlist)
        np_array = []
        for letter in point:
            try:
                np_array.append(values2[letter])
            except KeyError:
                print('nokey: ' + str(letter))
        while len(np_array) < 40:
            np_array.append(0)
        return np_array
