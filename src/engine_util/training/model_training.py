import pandas as pd
from engine_data.ground_truth_processor import GroundTruthProcessor
from sklearn.model_selection import train_test_split
from keras.layers import Activation
from keras.layers import Dense
from keras.layers import GRU
from keras.layers.wrappers import TimeDistributed
from keras.models import Sequential
from keras.optimizers import SGD
from keras.callbacks import History
from pylab import *
from sklearn.preprocessing import MinMaxScaler
from keras.models import model_from_json
from sklearn.metrics import confusion_matrix
from sklearn.metrics import recall_score
from sklearn.metrics import precision_score
from sklearn.metrics import f1_score
from sklearn.metrics import accuracy_score
from sqlalchemy import create_engine
import mysql.connector
import engine_config.configurations
import string
import datetime
import time
from tensorflow import set_random_seed
set_random_seed(2)

from engine_util.training.nc_model_helper import NamingConventionModelHelper 

class ModelTrainer:
    """ Class for training models with new data (currently only supports Keras Models) """

    def __init__(self, df, start_time, model_type='', epoch_num=500, batch_size=100, test_size=0.2, learning_rate=0.0001):
        """ Instantiate class with a datafile to train a model with """

        
        if isinstance(df, pd.core.frame.DataFrame) == False:
            raise TypeError("Error: input for df must be a pandas Dataframe")
            
        self._df = df
        
        if isinstance(epoch_num, int) == False:
            raise TypeError("epoch_num must be an Integer")

        if epoch_num <= 0:
            raise ValueError("epoch_num must be greater than 0")
            
        self._epoch_num = epoch_num
        
        if isinstance(batch_size, int) == False:
            raise TypeError("batch_size must be an Integer")
            
        if batch_size <= 0:
            raise ValueError("batch_size must be greater than 0")
            
        self._batch_size = batch_size
        
        if isinstance(test_size, float) == False:
            raise TypeError("test_size must be a Float")
            
        if test_size <= 0 or test_size >= 1:
            raise ValueError("test_size must be between 0 and 1 (exclusive)")
            
        self._test_size = test_size
        
        if isinstance(learning_rate, float) == False:
            raise TypeError("learning_rate must be a Float")
            
        if learning_rate <= 0:
            raise ValueError("learning_rate must be greater than 0")
            
        self._learning_rate = learning_rate
        
        if isinstance(start_time, str) == False:
            raise TypeError("start_time must be a string")
            
        try:
            datetime.datetime.strptime(start_time, '%Y-%m-%d %H:%M:%S')
            self._start_time = start_time
            
        except ValueError:
            raise ValueError("start_time in incorrect format, use %Y-%m-%d %H:%M:%S") 
        
        if isinstance(model_type, str) == False:
            raise TypeError("model_type must be a string")
            
        self._save_name = model_type
        
        self._train_x = []
        self._train_y = []
        self._test_x = []
        self._test_y = []
        self._nc_model_helper = NamingConventionModelHelper()
        self._runtime = None
        self._history = None
        self._score = None
        self._model = None
        self._engine = None
        self._table_name = None
        self._score_metrics = None
        self._connection_string = None
        


    #### PUBLIC FUNCTIONS ####

    def preprocess_data(self):
        """ Partition dataframe into x values and y values """
        
        model_data = self._get_df()
        model_data = model_data[['point_names','aspectvalue']]
        model_data = model_data.reset_index()
        x = self._prepare_x_values(model_data)
        y = self._prepare_y_values(model_data)
        result = pd.concat([x, y], axis=1, join='inner')
        self._df = result
        self._create_train_test()

        print("Data processed")

    def train(self):
        """ Train GRU model """

        train_X = np.asarray(self._get_train_x())
        train_Y = np.asarray(self._get_train_y())
        self._create_model(train_X, train_Y)
        model = self._get_model()

        # train the KERAS model
        start_time = time.time()
        self._history = model.fit(train_X, train_Y, validation_split=.08, epochs=self._get_epoch_num(), batch_size=self._get_batch_size())
        self._runtime = (time.time()-start_time)
        print("Training Complete")

        # save the model-- creates json file and h5 file that stores its weights
        self._save_model(model)

    def test(self):
        """ Test GRU model """

        test_X = np.asarray(self._get_test_x())
        test_Y = np.asarray(self._get_test_y())

        self._score = self._model.evaluate(test_X, test_Y, batch_size=20)
        print("%s: %.2f%%" % (self._model.metrics_names[1], self._score[1]*100))

    def calculate(self):
        """ Calculate score metrics """

        self._create_metrics_df()

        table_name = engine_config.configurations.table_name
        self._push_metrics_to_db(self.get_score_metrics(), table_name)

        
    def set_epoch_num(self, new_num):
        try:
            if isinstance(new_num, int) == False:
                raise TypeError
            
            if new_num <= 0:
                raise ValueError
            
            self._epoch_num = new_num
            
        except ValueError:
            print("Input must be greater than 0. Keeping current Epoch Number:", self._get_epoch_num())
            
        except TypeError:
            print("Invalid input type. Input Integer. Keeping current Epoch Number:", self._get_epoch_num())

        
    def set_batch_size(self, new_size):
        try:
            if isinstance(new_size, int) == False:
                raise TypeError
            
            if new_size <= 0:
                raise ValueError
            
            self._batch_size = new_size
            
        except ValueError:
            print("Input must be greater than 0. Keeping current Batch Size:", self._get_batch_size())
            
        except TypeError:
            print("Invalid input type. Input Integer. Keeping current Batch Size:", self._get_batch_size())


    def set_test_size(self, new_size):
        try:
            if isinstance(new_size, float) == False:
                raise TypeError
            
            if new_size <= 0 or new_size >= 1:
                raise ValueError
            
            self._test_size = new_size
            
        except ValueError:
            print("Input must be greater than 0 and less than 1. Keeping current Test Size:", self._get_test_size())
            
        except TypeError:
            print("Invalid input type. Input Float. Keeping current Test Size:", self._get_test_size())

    def set_learning_rate(self, new_rate):
        try:
            if isinstance(new_rate, float) == False:
                raise TypeError
            
            if new_rate <= 0:
                raise ValueError
            
            self._learning_rate = new_rate
            
        except ValueError:
            print("Input must be greater than 0. Keeping current Learning Rate:", self._get_learning_rate())
            
        except TypeError:
            print("Invalid input type. Input Float. Keeping current Learning Rate:", self._get_learning_rate())


    #### PRIVATE FUNCTIONS ####

    def _create_model(self, train_X, train_Y):
        """ Create new model """

        model = Sequential()
        model.add(Dense(100, input_dim=train_X.shape[1], init='normal'))
        model.add(Activation('relu'))
        model.add(Dense(100, init='normal'))
        model.add(Activation('relu'))
        model.add(Dense(100, init='normal'))
        model.add(Activation('relu'))
        model.add(Dense(train_Y.shape[1], init='normal'))
        model.add(Activation('softmax'))
        sgd = SGD(lr=self._get_learning_rate(), decay=1e-6, momentum=0.5, nesterov=True)
        model.compile(loss='categorical_crossentropy', optimizer=sgd, metrics=['accuracy'])

        self._model = model

    def _prepare_x_values(self, df):
        name_list = df.point_names.values
        encoded_df = self._nc_model_helper.point_list_converter(name_list)
        x = pd.DataFrame({'col':encoded_df})
        df['name'] = x['col']
        x = pd.DataFrame.from_records(encoded_df)
        return x

    def _prepare_y_values(self, df):
        y = pd.get_dummies(df.aspectvalue)
        return y

    def _create_train_test(self):
        """ take data and extract what we need for the test """

        df = self._get_df()
        t_size = self._get_test_size()

        train, test = train_test_split(df, test_size=t_size)

        # Now store the data
        self._train_x = train[train.columns[0:40]]
        self._train_y = train[train.columns[40:]]
        self._test_x = test[test.columns[0:40]]
        self._test_y = test[test.columns[40:]]

    def _save_model(self, model):
        """ Save model structure in json and weights in h5. Store trained model in self._model """
        ##Create saved_models dir if not existing already
        directory = os.path.dirname('/src/engine_util/pipeline/saved_models')
        if not os.path.exists(directory):
            os.makedirs(directory)
            print("Location Checked")
        model_json = model.to_json()
        with open("saved_models/"+self._get_save_name()  + ".json", "w") as json_file:
            json_file.write(model_json)

        model.save_weights("saved_models/"+self._get_save_name() + ".h5")
        print("Saved model ")
        
        self._model = model

    def _append_timestamp(self):
        """ append timestamp to model name for time specificity """
        
        timestamp = str(datetime.datetime.utcnow()).split(".")[0].replace(" ", "_")
        final_name = self._get_save_name() +  "_" + timestamp
        return final_name

    def _push_metrics_to_db(self, score_metrics, table_name):
        """ push the score metrics to db """

        self._table_name = table_name
        self._dataset = score_metrics
        try:
            self._connection_string = 'mysql+mysqlconnector://'+engine_config.configurations.sql_username+':'+ engine_config.configurations.sql_password+'@'+engine_config.configurations.sql_ip+':'+ engine_config.configurations.sql_port+'/'+engine_config.configurations.sql_db
            if self._engine != '':
                self._engine = create_engine(self._get_connection_string(), echo=False)
            self._dataset.to_sql(name = self._get_table_name(), con = self._get_engine(), if_exists = 'append', index = False)
        except:
            print("Error with SQL push")
        finally:
            print("Closing the connection")
            # Close the connection
        
        
        print("pushed db!")

    def _create_metrics_df(self):
        """ Convert model metrics to pandas dataframe """

        runtime, train_loss, train_accuracy, test_loss, test_accuracy, train_size, test_size, recall_scores, precision_scores, f1_score = self._get_model_metrics(self._get_train_x(),self._get_train_y(),self._get_test_x(),self._get_test_y(),self._get_model())
        metrics_dict = {'model_type':self._get_save_name(), "train_ts":self._start_time,'runtime': runtime, 'train_loss': train_loss, 'train_accuracy': train_accuracy, 'test_loss': test_loss, 'test_accuracy': test_accuracy, 'train_size': train_size, 'test_size': test_size, \
        'recall_scores': str(recall_scores), 'precision_scores': str(precision_scores), 'f1_score': str(f1_score), 'epochs': self._get_epoch_num(), \
        'batch_size': self._get_batch_size()}

        score_metrics_df = pd.DataFrame(data=metrics_dict, index=[0])
        self._score_metrics = score_metrics_df

    def _get_model_metrics(self, x_train, y_train,x_test,y_test,model):
        """ calculate the metrics for the current model """

        y_true = y_test.values.argmax(axis=-1)
        y_pred_prob = model.predict(x_test, batch_size=20, verbose=0)
        y_pred = y_pred_prob.argmax(axis=-1)

        # summarize history for accuracy
        score = self._get_score()
        runtime = self._get_runtime()
        train_loss = self._get_history().history['loss'][-1]
        train_accuracy = self._get_history().history['acc'][-1]
        test_loss = score[0]
        test_accuracy = score[1]
        train_size = x_train.shape[0]
        test_size = x_test.shape[0]
        recall_scores = recall_score(y_true ,y_pred, average=None)
        precision_scores = precision_score(y_true ,y_pred, average=None)
        f1_scores = f1_score(y_true, y_pred, average=None)

        return  runtime, train_loss, train_accuracy, test_loss, test_accuracy, train_size, test_size, recall_scores, precision_scores, f1_scores


    #### GETTERS ####

    def _get_score(self):
        return self._score

    def get_score_metrics(self):
        return self._score_metrics

    def _get_df(self):
        return self._df
    
    def _get_test_size(self):
        return self._test_size

    def _get_model(self):
        return self._model
    
    def _get_epoch_num(self):
        return self._epoch_num
    
    def _get_batch_size(self):
        return self._batch_size

    def _get_learning_rate(self):
        return self._learning_rate

    def _get_train_x(self):
        return self._train_x

    def _get_train_y(self):
        return self._train_y

    def _get_test_x(self):
        return self._test_x

    def _get_test_y(self):
        return self._test_y

    def _get_history(self):
        return self._history
    
    def _get_save_name(self):
        return self._save_name
    
    def _get_runtime(self):
        return self._runtime

    def _get_connection_string(self):
        return self._connection_string

    def _get_table_name(self):
        return self._table_name

    def _get_engine(self):
        return self._engine

