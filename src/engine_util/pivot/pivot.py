# -*- coding: utf-8 -*-
"""
Created on Fri Apr 28 14:40:13 2017

@author: h172036
"""

import sys

from pyspark import SparkConf, SparkContext
# from pyspark.sql import SQLContext
from pyspark.sql import HiveContext

# # Constants
APP_NAME = "pivot hive tables"


def createIntailQuery(tableName, guid):
    intialSelect = 'select item_datetime_str,item_id,CAST(item_numeric_value AS String) as item_numeric_value from ' + tableName
    wherestatement = """ where systemguid='""" + guid + "'" + """and year=""" + '2017' + """ and month=""" + '01'

    return intialSelect + wherestatement


def pivottable(sqlContext, tablename, querystatment, guid):
    dataframe = sqlContext.sql(querystatment)
    rdd_intial = dataframe.rdd
    rdd_points = rdd_intial.map(lambda x: (x[1]))
    # Taking out distinct columns
    rdd_points = rdd_points.distinct()
    count_ref = rdd_points.count()
    all_points = rdd_points.collect()
    all_points = sorted(all_points)
    rdd_dict = convert_list(all_points)
    # creating key value pairs as (time_stamp,(point_id,value))
    rdd_map = rdd_intial.map(lambda x: (x[0], x[1] + '|' + x[2]))

    rdd_key = rdd_map.reduceByKey(lambda a, b: a + '$' + b)

    # applying the method to pivot table in a format of (time_stamp,[val1,val2,val3 .... valn])
    rddtransform = rdd_key.map(lambda s: (s[0], insert_hash(s[1], rdd_dict, count_ref)))

    # convert the file into csv
    csv_lines = rddtransform.map(toCSVLine)
    csv_lines_final = csv_lines.map(lambda x: x.split(',')[0:count_ref + 1])
    # create schema for the dataframe
    lis = []
    for point in all_points:
        # replacing dot with underscore and hypen with double underscore,hive schema doesnot allow dot and hypens in the column names
        p = point.replace(".", "_").replace("-", "__")
        lis.append(str(p))

    # create dataframe with rdd and schema
    df_final = sqlContext.createDataFrame(csv_lines_final, lis)

    # register the dataframe as temp table
    df_final.registerTempTable("dfPivotTable")

    # createquery
    createquery = createhivetable("dfPivotTable", tablename, guid)

    # create the table
    sqlContext.sql(createquery)


def createhivetable(dftemptable_name, tablename, guid):
    guid = guid.replace("-", "_")
    createtablestatment = "create table " + tablename + "_pivot_CROWNTOWERJAN_" + guid
    endstatement = " as select * from " + dftemptable_name
    return createtablestatment + endstatement


def convert_list(lis):
    dict = {}
    for i in range(0, len(lis)):
        dict[lis[i]] = i
    return dict


def toCSVLine(data):
    return ','.join(str(d) for d in data)


def insert_hash(s, dict, count_ref):
    sr = s.split('$')
    elements = [''] * count_ref
    for i in range(0, len(sr)):
        if len(sr[i].split('|')) == 2:
            sp = sr[i].split('|')[0].rstrip('\n\t')
            sv = sr[i].split('|')[1].rstrip('\n\t')
            pos = dict[sp]
            elements[pos] = sv
    return ','.join(elements)


def main(sc, tablename, guid):
    # parse date in dd-mm-yy-hr
    # processdatelist = processdate.split('-')
    # call query creater
    statementquery = createIntailQuery(tablename, guid)

    sqlContext = HiveContext(sc)

    pivottable(sqlContext, tablename, statementquery, guid)


if __name__ == "__main__":
    # pass arguments pivot.py db.tablename guid 2017-12-28
    sc = SparkContext(conf=SparkConf())
    # Configure Spark
    tablename = sys.argv[1]
    guid = sys.argv[2]
    # pass date in yyyy-mm-dd
    # processdate = sys.argv[3]
    # Execute Main functionality
    main(sc, tablename, guid)

    # stoping the spark Context
    print 'stopping the spark context'
    sc.stop()
