#!/bin/bash

#Parameters
building_name=$1
system_guid=$2


printf "\n"
printf "Executing Step 1:\nReplicating Data from production-blob\n"
printf "\n"

replicate_data="hadoop distcp  wasbs://obs-production-data@salsbx01sparkdata.blob.core.windows.net/"${system_guid}"-timeseries/* wasbs://obs-production-data@salsbx01sparkdata.blob.core.windows.net/hive-data/systemguid="${system_guid}
echo ${replicate_data}
eval "${replicate_data}";

printf "\n"
printf "Executing Step 2:\nExtracting folder structure\n"
printf "\n"

folder_structure="hdfs dfs -lsr wasbs://obs-production-data@salsbx01sparkdata.blob.core.windows.net/hive-data/systemguid="${system_guid}"/ > obs_hdfs_"${building_name}".txt"
echo ${folder_structure}
eval "${folder_structure}";

printf "\n"
printf "Executing Step 3:\nGenerating Hive Partition Structure\n"
printf "\n"

hive_partition="python adding_partition_hive.py obs_hdfs_"${building_name}".txt obs_hdfs_"${building_name}"_OUT.hql "${building_name}
echo ${hive_partition}
eval "${hive_partition}";

printf "\n"
printf "Executing Step 4:\nOpening Beeline Connection and creating hive partition\n"
printf "\n"

beeline << EOF
!connect jdbc:hive2://zk0-sal04s.sentience.local:2181,zk1-sal04s.sentience.local:2181,zk2-sal04s.sentience.local:2181/;serviceDiscoveryMode=zooKeeper;zooKeeperNamespace=hiveserver2-hive2 ${username} ${password}
!run obs_hdfs_${building_name}_OUT.hql

EOF


printf "\n"
printf "Executing Step 6:\nPerforming Pivoting operation\n"
printf "\n"

execute_pivot="nohup python pivot.py hbt_datalake.obs_raw_v3 ${building_name} &"
echo ${execute_pivot}