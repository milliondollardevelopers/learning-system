import os
import glob
import os
import pandas as pd
import csv
import time
import pyodbc
from tensorflow import set_random_seed
from engine_util.training.model_training import ModelTrainer
from engine_data.ground_truth_processor import GroundTruthProcessor
#from azure.storage.blob import BlockBlobService
import engine_config.configurations
from time import gmtime, strftime

set_random_seed(2)

start_ts = strftime('%Y-%m-%d %H:%M:%S',gmtime())
start_time = strftime("%Y%m%d%H%M%S", gmtime())
gt_path = os.getcwd() + '/ground_truth.csv'

# Put this in evironment variable
container_name ='hbt-obs'
#block_blob_service = BlockBlobService(account_name='hbtcdtml',sas_token=os.environ['SAS_URI'])

def get_ground_truth():
    
    #cnxn = pyodbc.connect('DRIVER={Hortonworks Hive ODBC Driver};SERVER=Hive Server 2; Host = ' + os.environ['HOST'] + ';DATABASE=' + os.environ['DATABASE'] + ';UID=' + os.environ['MY_UID'] + ';PWD=' + os.environ['MY_PWD'] + ';port=443; Thrift Transport = HTTP;ssl=1;Authmech = 6',autocommit=1)
    #cnxn = pyodbc.connect('DRIVER={Hortonworks Hive ODBC Driver};SERVER=Hive Server 2; Host = ds01sbx.azurehdinsight.net;DATABASE=ds01_hbt_obs_groundtruth_sbx;UID=e526340@sentience.local;PWD=Turing.5432109;port=443; Thrift Transport = HTTP;ssl=1;Authmech = 6',autocommit=1)
    cursor = cnxn.cursor()
    rows=cursor.execute("select *  from ds01_hbt_obs_groundtruth_sbx.sensors;")
    start_time = time.time()
    with open(gt_path, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow([x[0] for x in cursor.description])  # column headers
        for row in rows:
            writer.writerow(row)



#get_ground_truth()
ground_truth_data = pd.read_csv(gt_path)
ground_truth_data.columns = ['model_id', 'point_names', 'aspectvalue', 'aspectclass', 'source']

# ground_truth_data['aspectclass'] = ground_truth_data['sensors.aspectclass']
# ground_truth_data['aspectvalue'] = ground_truth_data['sensors.aspectvalue']
# ground_truth_data['point_names'] = ground_truth_data['sensors.point_names']


aspectLabelClassList =['ControlPointFunctionType','MeasureType', 'SignalType', 'ElementType','ElementAssemblyType','DistributionLocationType', 'MaterialType','DistributionFunctionType', 'SignalDirectionType']

print('changed names ######')
print(list(ground_truth_data))
print(ground_truth_data.head())
print(len(ground_truth_data))


Score_df = pd.DataFrame() ## declare empty dataframe outside the loop
for aspectLabelClass in aspectLabelClassList:
    print("Training model for "+ aspectLabelClass +"\n")
    model_type_df = ground_truth_data[ground_truth_data.aspectclass == aspectLabelClass]

    # model_trainer = ModelTrainer(df=model_type_df, batch_size= 200 ,model_type = aspectLabelClass, epoch_num=1000,start_time=start_ts) #actual model
    model_trainer = ModelTrainer(df=model_type_df, batch_size= 1 ,model_type = aspectLabelClass, epoch_num=1,start_time=start_ts) ##test model
    model_trainer.preprocess_data()
    model_trainer.train()
    model_trainer.test()
   
    model_trainer.calculate()
    print(model_trainer.get_score_metrics())

    
    # create csv with the model scores and parameters.
    Score_df = Score_df.append(model_trainer.get_score_metrics(), ignore_index= True)
    Score_df.to_csv('../Model_Scores.csv', index = False)
    
    print("Training ended for "+ aspectLabelClass +"\n")
    # block_blob_service.create_blob_from_path(container_name, start_time+'/'+aspectLabelClass.replace(" ", "")+'.h5', "saved_models/"+aspectLabelClass+".h5")
    # block_blob_service.create_blob_from_path(container_name, start_time+'/'+aspectLabelClass.replace(" ", "")+'.json', "saved_models/"+aspectLabelClass+".json")
