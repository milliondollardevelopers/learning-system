##################################################################################################################################################################################################################
# Instructions for adding Hive Paritions on External Tables 
#
# 1. hdfs dfs -lsr wasbs://telemetry-data@hbtobsiotdliprod.blob.core.windows.net/ > obs_hdfs.txt
# 2. python adding_partition_hive.py obs_hdfs.txt obs_hdfs.hql ds01_hbt_obs_groundtruth_sbx raw_obs_telemetry wasbs://telemetry-data@hbtobsiotdliprod.blob.core.windows.net
# 3. beeline -u "jdbc:hive2://zk2-ds01sb.sentience.local:2181,zk3-ds01sb.sentience.local:2181,zk4-ds01sb.sentience.local:2181/;serviceDiscoveryMode=zooKeeper;zooKeeperNamespace=hiveserver2-hive2"
# 4. !run obs_hdfs.hql
##################################################################################################################################################################################################################


import re
import sys

blob_file_list = sys.argv[1]
hiveql_file = sys.argv[2]
database_name = sys.argv[3]
table_name = sys.argv[4]
blob_uri = sys.argv[5]

main_sql = "alter table "+database_name+"."+table_name+" add IF NOT EXISTS"

intermediate_sql = "partition ("
end_sql = ") location "
file_list = open(blob_file_list, 'r')
hql_file = open(hiveql_file, 'a+')
sqlfile = file_list.readlines()
final_count = 0

list1 = []
for line in sqlfile:
        if re.search('.csv', line):
            final_count += 1
            l = line.split('/')
            sub_list = l[-6:-3]
            some_str = ""
            end_str = ""
            cnt = 0
            for i in range(len(sub_list)):
                if i==0:
                    key = "sys_guid"
                    sub = sub_list[i].split('-')[:-1]
                    value = '-'.join(sub)
                else:
                    key = sub_list[i].split('=')[0]
                    value = sub_list[i].split('=')[1]            
                key = str(key)
                value = str(value)

                if cnt < 2:
                    if i == 0:
                        some_str += "%s='%s'," % (key, value)
                        end_str += "%s-timeseries/" % (value)
                    else:
                        some_str += "%s='%s'," % (key, value)
                        end_str += "%s=%s/" % (key, value)
                    cnt += 1
                else:
                    if i == 0:
                        some_str += "%s" % (key)
                        end_str += "%s/" % (value)
                    else:
                        some_str += "%s='%s'" % (key, value)
                        end_str +="%s=%s/" % (key, value)
            if final_count == 1:
                final_sql = main_sql + " " + intermediate_sql + "" + some_str + end_sql + blob_uri+ "%s" % end_str + ";"
                list1.append(final_sql)
                hql_file.write(final_sql + "\n")
            else:
                final_sql = main_sql + " " + intermediate_sql + "" + some_str + end_sql + blob_uri+ "%s" % end_str + ";"
                if final_sql not in list1 :
                    list1.append(final_sql)
                    hql_file.write(final_sql + "\n")
hql_file.close()
file_list.close()

