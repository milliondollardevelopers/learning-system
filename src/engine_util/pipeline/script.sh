#!/bin/sh

python << EOF

from engine_data.ts_model_training import ModelTrainer
from engine_data.ground_truth_processor import GroundTruthProcessor

gtp = GroundTruthProcessor(source="Fuseki")
# Load data for an array of sites
site_list = ['bcg_york',
             'bcg_curiest',
             'bcg_burjkhalifa',
             'bcg_nnswhealthca',
             'bcg_crownpodium',
             'bcg_darwincec',
             'bcg_emmarsouthridge',
             'bcg_etobicoke',
             'bcg_goldenvalley',
             'bcg_madridoffice',
             'bcg_longbayforensic',
             'bcg_longbayprison',
             'bcg_phoenixmaryvale',
             'bcg_mlccentre',
             'bcg_morrisplains',
             'bcg_mpha','bcg_nnswhealthgr','bcg_nnswhealthmu','bcg_princesstower','bcg_qcc','bcg_rialto',
        'bcg_richardsonplace','bcg_rockandrollhof','bcg_stardarling','bcg_phoenixtrevorbrowne','bcg_trillium','bcg_victoriancourts','bcg_wembley']
gtp.load_ground_truth(site_list)

aspectLabelClassList = ["Control Point Function Type", "Distribution Function Type", "Distribution Location Type", "Element Assembly Type", "Element Type", "Material Type", "Measure Type", "Signal Type"]

for aspectLabelClass in aspectLabelClassList:

    model_type_df = gtp.ground_truth_data[gtp.ground_truth_data.aspectClassLabel == aspectLabelClass]

    model_trainer = ModelTrainer(df=model_type_df, model_type=aspectLabelClass)
    model_trainer.preprocess_data()
    model_trainer.train()
    model_trainer.test()
    model_trainer.calculate()

EOF
