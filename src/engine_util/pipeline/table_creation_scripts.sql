--HDFS DATA SIZE TABLE:

CREATE EXTERNAL TABLE IF NOT EXISTS hdfs_data(size STRING, systemguid STRING ) 
COMMENT 'HDFS Data Size'
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ','
STORED AS TEXTFILE
LOCATION '/hbt/obs/availability';

--SITES TABLE:

CREATE EXTERNAL TABLE IF NOT EXISTS sites(region STRING, country STRING, district STRING, type STRING, model_id STRING,
systemguid STRING,asset_count int, point_count int, rule_count int, language string, licensed_point_count int, 
skyspark_id string, created string, data_rights_category int) 
COMMENT 'Site mapping table' 
ROW FORMAT DELIMITED FIELDS TERMINATED BY ','
STORED AS TEXTFILE LOCATION '/hbt/obs/sites';