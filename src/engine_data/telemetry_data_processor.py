import pandas as pd
import engine_data
import csv
import time
import pyodbc
from engine_config.mapping_dicts import measure_type_mapping,control_fn_type_mapping,material_fn_type_mapping,signal_fn_type_mapping

class TelemetryDataProcessor:

    def __init__(self,building_name,data_path):
        self.data_path = data_path
        self.building_name = building_name
    
    def load_data(self):
        print("Data Load: Loading pivoted data")
        self.pivoted_data_df = pd.read_csv(self.data_path + "hive_data/"+self.building_name + ".csv")
        print("Data Load: Ended")

    def load_ground_truth(self):
        print("Data Load: Loading ground truth information data")
        self.ground_truth_df = pd.read_csv(self.data_path + "ground_truth/"+self.building_name + "_ground_truth")
        print("Data Load: Ended")

    def clean_col_names(self,unwanted_strings):
        for unwanted_string in unwanted_strings:
            self.pivoted_data_df.columns = self.pivoted_data_df.columns.str.replace(unwanted_string, "")
        return self.get_data_head()

    def get_data_head(self):
        return self.pivoted_data_df.head()

    def clean_timestamp(self):
        print("Removing millisecond info in timestamps")
        self.pivoted_data_df['time_stamp'] = self.pivoted_data_df['time_stamp'].map(lambda x: str(x)[:-8])
        print("Data Representation cleanup: Ended")
        return self.get_data_head()

    def get_descriptive_stats(self):
        print("Descriptive Statistics Calculation: Started")
        self.desc_stat = self.pivoted_data_df.describe()
        self.desc_stat.to_csv("metrics/" + self.building_name + "_desc_stat.csv", header=True, index=False, na_rep=" ")
        print("Descriptive Statistics Calculation: Ended")
        return self.desc_stat
    
    def get_memory_stats(self):
        self.pivoted_data_df_mem_info = self.pivoted_data_df.info(memory_usage='deep')
        self.sensor_count = self.pivoted_data_df.shape[1] - 1
        self.ts_length_count = self.pivoted_data_df.shape[0]
        print("Sensor Count: " + str(self.sensor_count))
        print("Number of timestamps: " + str(self.ts_length_count))
        print("Memory Statistics: Ended")
    
    def missing_value_stats(self):
        mis_val = self.pivoted_data_df.isnull().sum()
        mis_val_percent = 100 * self.pivoted_data_df.isnull().sum() / len(self.pivoted_data_df)
        mis_val_table = pd.concat([mis_val, mis_val_percent], axis=1)
        mis_val_table_ren_columns = mis_val_table.rename(
            columns={0: 'Missing Values', 1: '% of Total Values'})
        mis_val_table_ren_columns = mis_val_table_ren_columns[
            mis_val_table_ren_columns.iloc[:, 1] != 0].sort_values(
            '% of Total Values', ascending=False).round(1)
        print ("Your selected dataframe has " + str(self.pivoted_data_df.shape[1]) + " columns.\n"
                                                                "There are " + str(mis_val_table_ren_columns.shape[0]) +
            " columns that have missing values.")

        mis_val_table_ren_columns.to_csv("metrics/" + self.building_name + "_missing_value_metrics.csv", header=True, index=False,
                                    na_rep=" ")
        return mis_val_table_ren_columns
    
    def merge_ground_truth(self):
        print("Pivoting the df: Started")
        pivoted_data_df_new = self.pivoted_data_df.set_index('time_stamp').stack()
        print(pivoted_data_df_new.head(20))
        df_new = pivoted_data_df_new.reset_index(name='point_value')
        df_new.rename(columns={'level_1': 'point_names'}, inplace=True)
        df_new.to_csv("metrics/" + self.building_name + "_pivoted_sample.csv", header=True, index=False, na_rep=" ")
        print(df_new.head())
        print("Pivoting the df: Ended")

        print("Merging Ground Truth and Pivoted Data: Started")
        self.ground_truth_df['point_names'] = self.ground_truth_df['point_names'].str.lower()
        self.ground_truth_df = self.ground_truth_df.drop(['propertyLabel', 'extension', 'propertyComment', 'Building'], axis=1)
        print(self.ground_truth_df.head())
        self.merged_training_data = df_new.merge(self.ground_truth_df, on="point_names")
        print(self.merged_training_data.head())
        print("Merging Ground Truth and Pivoted Data: Ended")

    
    def score_to_numeric(self, aspect_value,mapping):
            return mapping[aspect_value]

    def create_label_data(self):
        print("Creating type specific data : Started")

        mapping_list = [measure_type_mapping,control_fn_type_mapping,material_fn_type_mapping,signal_fn_type_mapping]
        mapping_names = ["Measure Model", "Control Function Model","Material Model", "Signal Model" ]

        building_data = []
        for mapping,name in zip(mapping_list, mapping_names):
            print("Creating Data for "+name +" : Started")
            aspect_data = self.merged_training_data[self.merged_training_data.NewAspectValue.isin(list(mapping.keys()))]
            aspect_data['NewAspectValue'] = aspect_data['NewAspectValue'].astype('category')
            aspect_data['Label'] = aspect_data['NewAspectValue'].apply(lambda aspect: self.score_to_numeric(aspect, mapping))
            aspect_data['Label'] = aspect_data['Label'].astype('category')
            aspect_data['Model'] = name

            aspect_data['Label']= aspect_data['Label'].astype(str)
            aspect_data['Model']= aspect_data['Model'].astype(str)
            aspect_data['NewAspectValue']= aspect_data['NewAspectValue'].astype(str)
            aspect_data['point_names']= aspect_data['point_names'].astype(str)
            aspect_data = aspect_data[aspect_data['Label'].notnull()] 
            building_data.append(aspect_data)
            print("Creating Data for "+name +" : Ended")

            print(aspect_data.head())

        self.labeled_training_data = pd.concat(building_data)
        print("Creating type specific data : Ended")

    
    def get_pivoted_data(self,driver_version,host, username, passkey, database, port, table_name):
        print("Fetching Pivoted Data. Please wait!")
        connection_string = 'DRIVER={'+str(driver_version)+'};SERVER=Hive Server 2; Host = '+str(host)+';DATABASE='+str(database)+';UID='+str(username)+';PWD='+str(passkey)+';port='+str(port)+'; Thrift Transport = HTTP;ssl=1;Authmech = 6'
        cnxn = pyodbc.connect(connection_string,autocommit=1)
        cursor = cnxn.cursor()
        start_time = time.time()

        query = "select *  from "+str(database)+"."+str(table_name)
        rows=cursor.execute(query)
        extracted_file_location = self.data_path + self.building_name + '.csv'
        with open(extracted_file_location, 'w', newline='') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow([x[0] for x in cursor.description])  # column headers
            for row in rows:
                writer.writerow(row)        
        print("Done Fetching:--- %s seconds ---" % (time.time() - start_time))
        print("Extracted file location: "+str(extracted_file_location))


    """Code added by Bhavtosh."""

    '''Functions to be used before creating 1440 row dataframe. (raw_sensor_data)'''
    # Count number of points for each sensor. input: df, output: stats
    def count_sensor_points(raw_sensor_data):
        pd.options.display.float_format = '{:.4f}'.format
        return raw_sensor_data.groupby('propertyLabel')['point_value'].sum().rank(ascending=True)

    # Represent all 'bad' values as NaNs.
    def change_bad_to_nans(raw_sensor_data):
        raw_sensor_data = raw_sensor_data.loc[raw_sensor_data.status == 'Bad', 'point_value'] = math.nan
        return raw_sensor_data

    # Count number of NaNs/ Bad values for sensors. (Sensors with 0 NaNs are excluded)
    def sensor_nan_count(raw_sensor_data):
        bad_df = raw_sensor_data[raw_sensor_data.status == 'Bad']
        return bad_df.groupby('propertyLabel')['status'].count()

    '''Functions to be used after creating 1440 row dataframe. (processed_sensor_data)'''

    # Count number of NaNs per row. This will help us remove those rows which have Nans above a certain threshold.
    def count_nans_per_row(processed_sensor_data):
        processed_sensor_data = processed_sensor_data.isnull().sum(axis=1)
        return processed_sensor_data

    # Remove rows that have NaNs beyond a threshold of k (0< k < 1)
    def remove_nan_rows(processed_sensor_data, k):
        table = processed_sensor_data.dropna(thresh=int(k * 720))
        return table

    # Replace remaining NaNs using interpolation.
    def replace_nans(processed_sensor_data):
        table = processed_sensor_data.interpolate(axis=1, limit_direction='both')
        return table


