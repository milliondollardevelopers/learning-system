import sys
import time, datetime
from pyspark import SparkConf, SparkContext
from pyspark.sql import HiveContext
from pyspark.sql.functions import unix_timestamp, current_timestamp
from pyspark.sql.types import *

""" get min, max, mean, and standard deviation for each sensor in each sguid """
def create_descriptive_stats(db, source, target, sguids):

    sqlContext = set_context()
    sqlContext = set_target_table(sqlContext, db, target)

    for sguid in sguids:
        query = "select item_name, item_value_numeric from " + source + " where tag_item_quality='Good' and sys_guid='" + sguid + "'"
        print(query)
        df = sqlContext.sql(query)
        df = df.na.fill(0)
        rdd = df.rdd.partitionBy(150)
        rdd = rdd.map(tuple)
        pairRDD = rdd.mapValues(lambda x: float(x))

        # it seems like this sguid is too large to cache
        if sguid!='2d55476f-c34f-4312-b756-bff84bb45348':
                pairRDD.cache()

        max_group = pairRDD.reduceByKey(max)
        min_group = pairRDD.reduceByKey(min)
        avg_group = pairRDD.mapValues(lambda v: (v, 1)).reduceByKey(lambda a,b: (a[0]+b[0], a[1]+b[1])).mapValues(lambda v: v[0]/v[1])
        #stddev_group = pairRDD.mapValues(lambda v: (1, v, v*v)).reduceByKey(lambda a,b: (a[0]+b[0], a[1]+b[1], a[2]+b[2])).mapValues(lambda v: (abs((v[2]/v[0] - (v[1]/v[0])**2))**0.5))

        max_dict = max_group.collectAsMap()
        mean_dict = avg_group.collectAsMap()

        stddev_group = pairRDD.map(lambda x: (x[0], (((x[1]-mean_dict.get(x[0]))**2), 1))).reduceByKey(lambda x,y: (x[0]+y[0], x[1]+y[1])).mapValues(lambda x: (x[0]/x[1])**0.5)
        stddev_dict = stddev_group.collectAsMap()

        if sguid!='2d55476f-c34f-4312-b756-bff84bb45348':
                pairRDD.unpersist()


        total_join = min_group.map(lambda tup: (tup[0], tup[1], max_dict.get(tup[0]), mean_dict.get(tup[0]), stddev_dict.get(tup[0]), sguid)).sortBy(lambda a: a[0])

        schema = StructType([
                StructField('PointName', StringType(), True),
                StructField('Min', FloatType(), True),
                StructField('Max', FloatType(), True),
                StructField('Mean', FloatType(), True),
                StructField('StdDev', FloatType(), True),
                StructField('Sguid', StringType(), True)
                ])

        dataframe = sqlContext.createDataFrame(total_join,schema)

        print("attempting to write data from " +  sguid  + " to hive table")
        dataframe.write.insertInto(target, overwrite=False)


""" helper function to set context """
def set_context():
    sc = SparkContext(conf=SparkConf().setAll([('spark.sechduler.mode', 'FAIR'), ('spark.executor.cores', '10'), ('spark.executor.memory', '2g'), ('spark.shuffle.service.enabled', 'true'), ('spark.dynamicAllocation.enabled', 'true')]))
    sqlContext = HiveContext(sc)
    sqlContext.setConf("mapreduce.input.fileinputformat.input.dir.recursive","true")
    sqlContext.setConf("hive.input.dir.recursive","true")
    sqlContext.setConf("hive.mapred.supports.subdirectories","true")
    sqlContext.setConf("hive.supports.subdirectories","true")
    sqlContext.setConf("hive.exec.dynamic.partition", "true")
    sqlContext.setConf("hive.exec.dynamic.partition.mode", "nonstrict")

    return sqlContext


""" helper function to specify database and clear table """
def set_target_table(context, db, target):
    sqlContext = context
    sqlContext.sql("use " + db)
    sqlContext.sql("truncate table " + target)

    return sqlContext


### run code ###
start_time = time.time()
sguids = ["a786d94a-337c-4a94-ab25-720fe929411c", "cef46f2b-5f4d-4cad-9f37-45ebd8e6a939", "b422fcd0-564a-415c-83e8-3a41bbe21b24", "7ce751ab-18a4-43d9-9865-8d1c13be8983", "ecf0ea6e-13fe-4f10-8578-28e5b8087dd7", "839e69be-ee3b-4aca-aa22-2df933170475", "93ebaf1c-e7f0-4a65-830f-65d0aed3906f", "2d55476f-c34f-4312-b756-bff84bb45348"]
long_sguid = ["2d55476f-c34f-4312-b756-bff84bb45348"]
create_descriptive_stats("ds01_hbt_obs_groundtruth_sbx", "raw_obs_telemetry", "descriptive_stats_ts", sguids)
elapsed_time = time.time() - start_time
print("Time Elapsed: " + str(elapsed_time))

