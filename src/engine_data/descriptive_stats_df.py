import sys
from time import gmtime, strftime
import pyspark
from pyspark.sql import SparkSession, functions, Window
from pyspark.sql.functions import col, mean as _mean, stddev as _stddev, min as _min, max as _max, count as _count, \
    struct, udf

from pyspark.sql import SparkSession, functions, HiveContext
from pyspark.sql.types import StructType, StructField, IntegerType, StringType, DoubleType
from itertools import chain

'''

This file provides a method clean_and_pivot_data which will provide imputation of the data as well as pivot the data. 
The method and Descriptive Status class will remove all null values and values above and below upper and lower bound 
per each sensor.  

************************************************************************************************
    Below is an example on how to use this file form your current codebase.      
************************************************************************************************



print("Creating the Conf")
sc = pyspark.SparkContext(conf=pyspark.SparkConf().setAll([('spark.sechduler.mode', 'FAIR'), 
('spark.shuffle.service.enabled', 'true'), ('spark.dynamicAllocation.enabled', 'true')]))
sc.setLogLevel("WARN")

sqlContext = HiveContext(sc)
sqlContext.setConf("mapreduce.input.fileinputformat.input.dir.recursive", "true")
sqlContext.setConf("hive.input.dir.recursive", "true")
sqlContext.setConf("hive.mapred.supports.subdirectories", "true")
sqlContext.setConf("hive.supports.subdirectories", "true")
sqlContext.setConf("hive.exec.dynamic.partition", "true")
sqlContext.setConf("hive.exec.dynamic.partition.mode", "nonstrict")
sqlContext.sql("use obs_hive_db")
sqlContext.sql("truncate table month_meta_data")

start_date = "20181130"
end_date = "20181230"
systemguid = "7a13ef07-8c11-43d8-bed2-19cbd1d338c0"
table_name = "obs_telemetry"
database = "obs_hive_db"
output_dir = "/tmp/"

clean_and_pivot_data(start_date, end_date, systemguid, table_name, database, output_dir)

sc.stop()
sys.exit()



'''


class DescriptiveStatsDf:
    '''
        This class provides methods for getting the data from the database, generating meta data for the imputation
        and pivoting. The class is used by the clean and pivot data method.
    '''

    _clean_raw_df = None

    def __init__(self, start_date, end_date, systemguid, table_name, database, output_dir):
        '''

        :param start_date: Used as the start date for the query in the hive database
        :param end_date: Used as the end date for the query in the hive database
        :param systemguid: Used for the building in the query for the hive database
        :param table_name: Used for specific build table to query in the hive database
        :param database: Used for the actual hive database to be used.
        :param output_dir: Used to output the file once the process is finished.

        '''

        self.start_date = start_date
        self.end_date = end_date
        self.end_date = end_date
        self.systemguid = systemguid
        self.table_name = table_name
        self.database = database
        self.output_dir = output_dir

    def _get_prep_data(self):
        '''
            This method is used to query the deatabase and set it up for the rest of the processes. Metadata and
            clean data methods in the class uses this data

        :return: sensor and value df which is used by metadata method.
        '''
        sqlContext.sql("use " + self.database)

        query = "select * from " + self.table_name + " where systemguid ='" + self.systemguid + "' and (" \
                                                                                                "processeddate_utc>'" \
                + self.start_date + "' and processeddate_utc<'" + self.end_date + "')"

        print("query: " + query)

        _clean_raw_df = sqlContext.sql(query)


        _clean_raw_df = _clean_raw_df.select('datetime_utc', 'epoch_utc', 'stype', 'sguid', 'item_name',
                                             'item_value_numeric', 'item_value_str',
                                             'tag_item_quality', 'tag_item_tzoffset', 'systemtype', 'systemguid',
                                             'processeddate_utc').where(
            "tag_item_quality='Good' and item_value_numeric<1000000000")

        _clean_raw_df = _clean_raw_df.cache()
        print(strftime('%Y-%m-%d %H:%M:%S', gmtime()))
        print("Start the calculations")

        # sensor_and_value_df = df.select("item_name", "item_value_numeric").where(
        #   "tag_item_quality='Good' and item_value_numeric<1000000000")

        self._clean_raw_df = _clean_raw_df

        sensor_and_value_df = _clean_raw_df.select("item_name", "item_value_numeric")

        return sensor_and_value_df

    def clean_data(self):
        '''
            This is the last step of the process. This method will use the matadata generated per sensor and remove
            nulls, values above uppper bound and lower bound. Also, the method pivotes the data, gzips into a csv file.

        :return: None
        '''

        _clean_raw_df = self._clean_raw_df.withColumn("combined_value", struct("item_name", "item_value_numeric"))
        _clean_raw_df.show()

        print(strftime('%Y-%m-%d %H:%M:%S', gmtime()))
        print("Cleaning  using dataframe.")

        _clean_raw_df = _clean_raw_df.withColumn("combined_value", clean_item_value_numeric_udf("combined_value"))
        _clean_raw_df.show(700)

        _clean_raw_df = _clean_raw_df.select('datetime_utc', 'epoch_utc', 'stype', 'sguid', 'item_name',
                                             'combined_value', 'item_value_str',
                                             'tag_item_quality', 'tag_item_tzoffset', 'systemtype', 'systemguid',
                                             'processeddate_utc')

        _clean_raw_df = _clean_raw_df.withColumnRenamed('combined_value', 'item_value_numeric')
        _clean_raw_df.show()

        sampled_df = _clean_raw_df.select('datetime_utc', 'systemguid', 'item_name', 'item_value_numeric')
        sampled_df.show()

        sampled_df = sampled_df.withColumn('timestamp', sampled_df.datetime_utc.cast("timestamp"))
        sampled_df = sampled_df.withColumn('date', functions.date_format(sampled_df.timestamp, 'yyyy-MM-dd'))
        sampled_df = sampled_df.withColumn('hour',
                                           functions.date_format(sampled_df.timestamp, 'HH').cast(IntegerType()))
        sampled_df = sampled_df.withColumn('minute',
                                           functions.date_format(sampled_df.timestamp, 'mm').cast(IntegerType()))
        sampled_df = sampled_df.withColumn('minute_of_day', (sampled_df.hour * 60 + sampled_df.minute))

        sampled_df.show()

        pivoted_df = sampled_df.groupBy('systemguid', 'item_name', 'date').pivot('minute_of_day').agg(
            {'item_value_numeric': 'sum'}).orderBy('systemguid', 'item_name', 'date')

        pivoted_df.show()

        file_name = self.systemguid + "_" + self.start_date + "_" + self.end_date

        file_name_path = self.output_dir + file_name

        pivoted_df.write.format("com.databricks.spark.csv").option("codec", "org.apache.hadoop.io.compress.GzipCodec") \
            .save(file_name_path + '.gzip')

    def convert_dataframe_to_dictionary(self, dataframeValues, key_name, value_name):
        '''
        This method takes a dataframe, key column name and value column name and returns a dictionary using those
        values from the dataframe.

        :param dataframeValues:
        :param key_name:
        :param value_name:
        :return: dictionary
        '''

        list_values = map(lambda row: row.asDict(), dataframeValues.collect())
        dictionary = {values[key_name]: values[value_name] for values in list_values}
        return dictionary

    def get_meta_data(self):
        '''

        This method uses the get_pre_data method to query the database and setup the data for each process. Also,
        this method generates mean, max, min, std, count and med values for each sensor. This information is used by
        other methds to clean the data.

        :return: metadata dataframe
        '''

        sensor_and_value_df = self._get_prep_data()

        sensor_and_value_df = sensor_and_value_df.filter(col("item_name").isNotNull())

        print("Printing Sensor and Value DF")
        print(strftime('%Y-%m-%d %H:%M:%S', gmtime()))

        sensor_and_value_df.show()

        grp_window = Window.partitionBy('item_name')
        magic_percentile = functions.expr('percentile_approx(item_value_numeric, 0.5)')

        print(strftime('%Y-%m-%d %H:%M:%S', gmtime()))
        print("Getting meta_data")
        meta_data = sensor_and_value_df.groupBy("item_name").agg(_mean("item_value_numeric").alias("mean"),
                                                                 _stddev("item_value_numeric").alias("std"),
                                                                 _min("item_value_numeric").alias("min"),
                                                                 _max("item_value_numeric").alias("max"),
                                                                 _count("item_value_numeric").alias("count"),
                                                                 magic_percentile.alias('med'))
        meta_data.show()

        print(strftime('%Y-%m-%d %H:%M:%S', gmtime()))
        print("Getting upper_bound")

        meta_data = meta_data.withColumn("upper_bound", struct("std", "mean"))
        meta_data.show()

        meta_data = meta_data.withColumn("upper_bound", calculate_upper_bound_udf("upper_bound"))
        meta_data.show()

        print(strftime('%Y-%m-%d %H:%M:%S', gmtime()))
        print("Getting lower_bound")

        meta_data = meta_data.withColumn("lower_bound", struct("std", "mean"))
        meta_data.show()

        meta_data = meta_data.withColumn("lower_bound", calculate_lower_bound_udf("lower_bound"))
        meta_data.show()

        meta_data.cache()

        return meta_data


def clean_item_value_numeric(numeric_item_values):
    '''
        This method used as a UDF method to remove nulls, values above upper bound and values below lower bound per
        each row in column numeric_item_values
    :param numeric_item_values:
    :return:
    '''
    numeric_item_value = numeric_item_values[1]
    item_name = numeric_item_values[0]

    median_value_dict.get(item_name)

    if item_name != None:

        median = median_value_dict.get(item_name)
        upper_bound = upper_bound_dict.get(item_name)
        lower_bound = lower_bound_dict.get(item_name)

        if (median != None):
            median = float(median)

        if (upper_bound != None):
            upper_bound = float(upper_bound)

        if (lower_bound != None):
            lower_bound = float(lower_bound)

        if ((median != None) and (upper_bound != None) and (lower_bound != None)):
            if (numeric_item_value == None):
                numeric_item_value = median

            elif (numeric_item_value > upper_bound):
                numeric_item_value = median
            elif (numeric_item_value < lower_bound):
                numeric_item_value = median
        else:
            if (median != None):
                if (numeric_item_value == None):
                    numeric_item_value = median
            else:
                if (numeric_item_value == None):
                    numeric_item_value = 0.0

    return numeric_item_value


def calculate_upper_bound(standard_deviation_mean):
    '''
    This method is used as a UDF to calculdate the upper bound for each sensor
    :param standard_deviation_mean:
    :return:
    '''
    standard_deviation = standard_deviation_mean[0]
    mean = standard_deviation_mean[1]
    return (standard_deviation * 2) + mean


def calculate_lower_bound(standard_deviation_mean):
    '''
    This method is used as a UDF to calculdate the lower bound for each sensor
    :param standard_deviation_mean:
    :return:
    '''
    standard_deviation = standard_deviation_mean[0]
    mean = standard_deviation_mean[1]
    return mean - (standard_deviation * 2)


def clean_and_pivot_data(start_date, end_date, systemguid, table_name, database, output_dir):
    '''
    This is the method that can be used by classes and/or python files to run cleaning code. The method usese the
    class and all of the methods to clean and pivot the data.

    :param start_date:
    :param end_date:
    :param systemguid:
    :param table_name:
    :param database:
    :param output_dir:
    :return:
    '''
    global calculate_upper_bound_udf
    global calculate_lower_bound_udf
    global clean_item_value_numeric_udf

    global median_value_dict
    global upper_bound_dict
    global lower_bound_dict
    global meta_data

    calculate_upper_bound_udf = udf(calculate_upper_bound)
    calculate_lower_bound_udf = udf(calculate_lower_bound)

    dsDF = DescriptiveStatsDf(start_date, end_date, systemguid, table_name, database, output_dir)
    meta_data = dsDF.get_meta_data()

    print(strftime('%Y-%m-%d %H:%M:%S', gmtime()))
    print("Getting median_value_dict ")

    median_value_dict = dsDF.convert_dataframe_to_dictionary(meta_data, "item_name", "med")
    print(strftime('%Y-%m-%d %H:%M:%S', gmtime()))
    print("Getting upper_bound_dict ")

    print(median_value_dict)

    upper_bound_dict = dsDF.convert_dataframe_to_dictionary(meta_data, "item_name", "upper_bound")
    print(strftime('%Y-%m-%d %H:%M:%S', gmtime()))
    print("Getting lower_bound_dict")

    lower_bound_dict = dsDF.convert_dataframe_to_dictionary(meta_data, "item_name", "lower_bound")

    clean_item_value_numeric_udf = udf(clean_item_value_numeric)

    dsDF.clean_data()
