import sys
import time
from pyspark import SparkConf, SparkContext
from pyspark.sql import HiveContext
from pyspark.sql.functions import current_date, datediff, lit
from pyspark.sql.types import IntegerType, DateType
from datetime import datetime

def create_quality_stats(db, source, target, sguids):

    sqlContext = set_context()
    sqlContext = set_target_table(sqlContext, db, target)

    for sguid in sguids:
        query = 'select item_name, count(item_name) as total_count, sum(case tag_item_quality when "Bad" then 1 else 0 end) as bad_count, min(datetime_utc) as first_timestamp, max(datetime_utc) as last_timestamp from ' + source + ' where sys_guid="' + sguid + '" group by item_name order by item_name asc'
        print(query)
        df = sqlContext.sql(query)
        dataframe = df.withColumn('total_days', datediff(df.last_timestamp, df.first_timestamp))
        dataframe = dataframe.withColumn('bad_percentage', dataframe.bad_count.cast(IntegerType()) / dataframe.total_count.cast(IntegerType()) * 100)
        dataframe = dataframe.drop("bad_count")
        dataframe = dataframe.withColumn('sys_guid', lit(sguid))


        print("attempting to write data from " +  sguid  + " to hive table")
        dataframe.write.insertInto(target, overwrite=False)



""" helper function to set context """
def set_context():
    sc = SparkContext(conf=SparkConf().setAll([('spark.sechduler.mode', 'FAIR'), ('spark.executor.cores', '10'), ('spark.shuffle.service.enabled', 'true'), ('spark.dynamicAllocation.enabled', 'true')]))
    sqlContext = HiveContext(sc)
    sqlContext.setConf("mapreduce.input.fileinputformat.input.dir.recursive","true")
    sqlContext.setConf("hive.input.dir.recursive","true")
    sqlContext.setConf("hive.mapred.supports.subdirectories","true")
    sqlContext.setConf("hive.supports.subdirectories","true")
    sqlContext.setConf("hive.exec.dynamic.partition", "true")
    sqlContext.setConf("hive.exec.dynamic.partition.mode", "nonstrict")

    return sqlContext


""" helper function to specify database and clear table """
def set_target_table(context, db, target):
    sqlContext = context
    sqlContext.sql("use " + db)
    sqlContext.sql("truncate table " + target)

    return sqlContext


### run code ###
start_time = time.time()
sguids = ["a786d94a-337c-4a94-ab25-720fe929411c", "cef46f2b-5f4d-4cad-9f37-45ebd8e6a939", "b422fcd0-564a-415c-83e8-3a41bbe21b24", "7ce751ab-18a4-43d9-9865-8d1c13be8983", "ecf0ea6e-13fe-4f10-8578-28e5b8087dd7", "839e69be-ee3b-4aca-aa22-2df933170475", "93ebaf1c-e7f0-4a65-830f-65d0aed3906f", "2d55476f-c34f-4312-b756-bff84bb45348"]
create_quality_stats("ds01_hbt_obs_groundtruth_sbx", "raw_obs_telemetry", "data_quality_stats_ts", sguids)
elapsed_time = time.time() - start_time
print("Time Elapsed: " + str(elapsed_time))
