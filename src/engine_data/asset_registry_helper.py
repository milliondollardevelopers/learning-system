import requests
import json
import json
from pprint import pprint
from graphqlclient import GraphQLClient
import pandas as pd
import engine_config.configurations

class AssetRegistryHelper:

    def __init__(self, model_id):

        self.client = None
        # Sentience Asset Registry Configuration
        self.tenant_authorization_ep = engine_config.configurations.tenant_authorization_ep
        self.resource = engine_config.configurations.resource
        self.client_id = engine_config.configurations.client_id
        self.client_secret = engine_config.configurations.client_secret
        self.tenant_name = engine_config.configurations.tenant_name
        self.model_id = model_id

        # Asset Registry URL
        self.graphql_client_url = 'https://'+self.tenant_name+'.testqa-cbp.honeywell.com/api/models/'+self.model_id+'/graphql'

    def get_token(self):
        try:
            self.url = self.tenant_authorization_ep
            self.headers = {}
            self.payload = {'resource':self.resource,
                    'scope':'openid',
                    'grant_type':'client_credentials',
                    'client_id': self.client_id ,
                    "client_secret":self.client_secret
            }

            response = requests.request("POST", self.url, data=self.payload, headers=self.headers,verify=False)
            response_json = json.loads(response.text)
            access_token = response_json['access_token']
            authorization = "Bearer " + access_token
            return authorization
        except Exception as e:
            print("Error getting a token")
            print(e)


    def authorize_graphql(self):
        try:
            self.client = GraphQLClient(self.graphql_client_url)
            self.client.inject_token(self.get_token())
        except Exception as e:
            print("Error authorizing GraphQL client")
            print(e)


    def execute_query(self,query):
        try:
            self.result = self.client.execute(query)
            return self.result
        except Exception as e:
            print("Error executing the GraphQL query")
            print(e)
