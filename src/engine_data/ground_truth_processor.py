import json
import pandas as pd
import requests
import mysql.connector
import engine_config.configurations
import mysql.connector
from sqlalchemy import create_engine
from engine_data.asset_registry_helper import AssetRegistryHelper

class GroundTruthProcessor:
    """Processor for loading, cleaning, and analyzing ground truth data.
    """

    def __init__(self, source):
        """Construct a new ground truth processor.
        Args:
        source: Location to extract ground truth data from.  We can use 
            sources like 'Fuseki','Asset Registry' or 'SentienceGroundTruthStore'.
        """

        self.source = source
        self._url = None
        self._r = None
        self._data = None
        self.ground_truth_data = None
        self._ground_truth_data = None
        self.distinct_sensors = None
        self.total_aspect_class_type = None
        self.total_distinct_class_types = None
        self.instances_each_class_category = None
        self._engine = None
        

    def _format_skypark_ground_truth(self,tag_name, point_list,_site_id):
        self._ground_truth_data = pd.DataFrame({'propertyLabel':point_list})
        self._ground_truth_data['aspectLabel'] = tag_name
        self._ground_truth_data['building_id'] = _site_id
        self._ground_truth_data['aspectClassLabel'] = 'Mesaure Type'

        return self._ground_truth_data

    def load_ground_truth(self, site_id_list, tag_list=[],class_label_list = []):
        """Extract Ground Truth Data Frame"""
    
        self._ground_truth_dfs = []
        for _site_id in site_id_list:
            if self.source == "Fuseki":
                try:
                    self._url = str(
                        engine_config.configurations.fuseki_ground_truth_url) + "/OntologyAPI/retrieval/runSPARQLPredefJSON?dataset=" + _site_id + "&queryName=getGroundTruth"
                    self._r = requests.get(self._url)
                    self._data = json.loads(self._r.text)
                    self._ground_truth_data = pd.DataFrame(data=self._data['results']['bindings'])
                    self._ground_truth_data = self._ground_truth_data[(
                            self._ground_truth_data.aspectClassLabel.notnull() & self._ground_truth_data.aspectLabel.notnull() & self._ground_truth_data.propertyComment.notnull() & self._ground_truth_data.propertyLabel.notnull())]
                    self._ground_truth_data['aspectClassLabel'] = self._ground_truth_data['aspectClassLabel'].map(
                        lambda x: x['value'])
                    self._ground_truth_data['aspectLabel'] = self._ground_truth_data['aspectLabel'].map(lambda x: x['value'])
                    self._ground_truth_data['propertyLabel'] = self._ground_truth_data['propertyLabel'].map(
                        lambda x: x['value'])
                    self._ground_truth_data['propertyComment'] = self._ground_truth_data['propertyComment'].map(
                        lambda x: x['value'])
                    self._ground_truth_data['building_id'] = _site_id
                    self._ground_truth_dfs.append(self._ground_truth_data)
                except:
                    print("Fuseki: Error loading data from " + self.source + " for " +_site_id) 
            elif self.source == "Skyspark":
                try:
                    for tag in tag_list:
                        self._url = str(
                            engine_config.configurations.skyspark_ground_truth_url) + "/"+_site_id + "/"+tag
                        self._r = requests.get(self._url,verify=False)
                        self._data = json.loads(self._r.text)
                        self._ground_truth_data = self._format_skypark_ground_truth(tag,self._data,_site_id)
                        self._ground_truth_dfs.append(self._ground_truth_data)
                except:
                        print("Skyspark: Error loading data from " + self.source + " for " +_site_id) 
            elif self.source == "AssetRegistry":
                try:
                    self._asset_reg_helper = AssetRegistryHelper(_site_id)
                    self._asset_reg_helper.authorize_graphql()
                    self._ground_truth = pd.DataFrame()

                    for label in class_label_list:
                        self._query = '''
                            {
                            coreProperties(first: 2500,filter: {isDescribedByRole: {hasPropertyRoleAspects_contains_all: [{name_matches: ".+'''+ label+'''", value_matches: ".+"}]}}) {
                                id
                                isDescribedByRole {
                                label
                                hasPropertyRoleAspects{
                                    name
                                    value
                                }
                                }
                            }
                            }
                            '''
                        self._result = self._asset_reg_helper.execute_query(self._query)
                        self._data = json.loads(self._result)
                        self._coreProperties = self._data["data"]["coreProperties"]
                        print(self._coreProperties)
                        for _coreProperty in self._coreProperties:
                            _propertyRoleAspects =  _coreProperty["isDescribedByRole"]['hasPropertyRoleAspects']
                            for _propertyRoleAspect in _propertyRoleAspects:
                                if(_propertyRoleAspect['name'].find(label) != -1):
                                    _prop_label = _coreProperty['id'].split("#")[1]
                                    self._ground_truth = self._ground_truth.append({'propertyLabel':_prop_label , 'aspectClassLabel':_propertyRoleAspect['name'].split("#")[1] , 'aspectLabel': _propertyRoleAspect['value'].split("#")[1]}, ignore_index=True)
                    self._ground_truth_dfs.append(self._ground_truth)
                except Exception as e:
                    print("Error loading data from Asset Registry")
                    print(e)

        self.ground_truth_data = pd.concat(self._ground_truth_dfs, ignore_index=True)


    def calculate_ground_truth_summary(self):
        """Get Ground Truth Data Summary"""
        try:
            self.distinct_sensors = self.ground_truth_data.propertyLabel.nunique()
            self.total_aspect_class_type = self.ground_truth_data['aspectClassLabel'].unique()
            self.total_distinct_class_types = self.ground_truth_data.groupby(['aspectClassLabel'])[
                'aspectClassLabel'].count()
            self.instances_each_class_category = self.ground_truth_data.groupby(
                ['aspectClassLabel', 'aspectLabel']).count()
            if 'propertyComment' in self.instances_each_class_category.columns:
                self.instances_each_class_category.drop('propertyComment', axis=1, inplace=True)
            if 'building_id' in self.instances_each_class_category.columns:
                self.instances_each_class_category.drop('building_id', axis=1, inplace=True)
            self.instances_each_class_category = self.instances_each_class_category.reset_index(level=['aspectClassLabel', 'aspectLabel'])
            self.instances_each_class_category.rename(columns={'aspectClassLabel': 'aspect_class'}, inplace=True)
            self.instances_each_class_category.rename(columns={'aspectLabel': 'aspect_label'}, inplace=True)
            self.instances_each_class_category.rename(columns={'propertyLabel': 'total_instances'}, inplace=True)
        except Exception as e:
            print("Error calculating ground truth summary")
            print(e)

    
    def update_ground_truth_stats_table(self):
        """Method to push ground truth statistics data to SQL """

        self.update_ground_truth_db(self.instances_each_class_category, 'instance_count_by_class')

    def update_ground_truth_table(self):
        """Method to push ground truth data to SQL """
        # Make sure your max_allowed_packet=8073741824 is large 

        self.update_ground_truth_db(self.ground_truth_data, 'ground_truth_data')
  
    def update_ground_truth_db(self, dataset, table_name):
        """Method to push ground truth data to SQL """

        self._table_name = table_name
        self._dataset = dataset
        try:
            self._connection_string = 'mysql+mysqlconnector://'+engine_config.configurations.sql_username+':'+ engine_config.configurations.sql_password+'@'+engine_config.configurations.sql_ip+':'+ engine_config.configurations.sql_port+'/'+engine_config.configurations.sql_db
            if self._engine != '':
                self._engine = create_engine(self._connection_string, echo=False)
            self._dataset.to_sql(name = self._table_name, con = self._engine, if_exists = 'replace', index = False)
        except:
            print("Error with SQL push")
        finally:
            print("Closing the connection")
            self._engine.dispose()
