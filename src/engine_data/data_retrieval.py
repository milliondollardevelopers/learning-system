# -*- coding: utf-8 -*-
""" Spark Code to retreive sample data from HBT Data Lake.

This module is intented to extract raw csv files for data analysis purpose

Example:
    Examples for extracting the data for a particular system guid::

        $ python data_retrieval.py --systemguid 0552266c-3626-4aca-97a5-7cf04cc8130c --processed_date_start 20181130 --processed_date_end 20181230 --dump_location "/tmp"

Todo:
    * Data extraction module TODOs

"""

import sys
import time
from pyspark import SparkConf, SparkContext
from pyspark.sql import HiveContext
import pyspark.sql.functions as F
from pyspark.sql.types import IntegerType, DateType
from datetime import datetime
import argparse


def set_context():
    """ Helper function to set sqlContext """

    sc = SparkContext(conf=SparkConf().setAll([('spark.scheduler.mode', 'FAIR'),('spark.shuffle.service.enabled', 'true'), ('spark.dynamicAllocation.enabled', 'true'),("spark.ui.showConsoleProgress", "true")]))
    sc.setLogLevel("WARN")
    sqlContext = HiveContext(sc)
    sqlContext.setConf("mapreduce.input.fileinputformat.input.dir.recursive","true")
    sqlContext.setConf("hive.input.dir.recursive","true")
    sqlContext.setConf("hive.mapred.supports.subdirectories","true")
    sqlContext.setConf("hive.supports.subdirectories","true")
    sqlContext.setConf("hive.exec.dynamic.partition", "true")
    sqlContext.setConf("hive.exec.dynamic.partition.mode", "nonstrict")

    return sqlContext

def extract_data(sqlContext,database,systemguid, processed_date_start,processed_date_end,dump_location):
    """ Helper function to set sqlContext """

    query = "select * from "+database+" where systemguid ='"+systemguid+"' and (" \
            "processeddate_utc>'"+processed_date_start+"' and processeddate_utc<'"+processed_date_end+"')"
    df = sqlContext.sql(query)
    df.write.format("csv").save(dump_location+"/"+systemguid+"_"+processed_date_start+"_"+processed_date_end)

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("--systemguid", help="System GUID.")
    parser.add_argument("--processed_date_start", help="Processed date start in YYYYMMDD")
    parser.add_argument("--processed_date_end", help="Processed date end in YYYYMMDD")
    parser.add_argument("--database", help="Database name")
    parser.add_argument("--table", help="Table name")
    parser.add_argument("--dump_location", help="Location to dump data to")
    args = parser.parse_args()
    if args.systemguid:
        systemguid = args.systemguid

    if args.database:
        database = args.database   

    if args.processed_date_start:
        processed_date_start = args.processed_date_start

    if args.processed_date_end:
        processed_date_end = args.processed_date_end

    if args.dump_location:
        dump_location = args.dump_location

    sqlContext = set_context()
    extract_data(sqlContext,database,systemguid, processed_date_start,processed_date_end,dump_location)

if __name__ == "__main__": main()

