from setuptools import setup, find_packages

setup(
    name="cdt-learning-system",
    version="0.1.0",
    author="HBT Turing Team",
    author_email="kushagra.thakur@honeywell.com",
    description="Package for retraining Deep Neural Net Models",
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Topic :: Scientific/Engineering :: Artificial Intelligence'
    ],
    package_dir={'': 'src'},
    packages=find_packages("src"),
    test_suite='nose.collector',
    test_require=['nose'],
    package_data={'engine_data': ['*.csv'], 'training_data': ['*.csv'], 'engine_util': ['*.sh']},
    install_requires=['pandas', 'requests','mysql-connector']
)
