import sys
import time, datetime
from pyspark import SparkConf, SparkContext
from pyspark.sql import HiveContext
from pyspark.sql.functions import unix_timestamp, current_timestamp, isnan
from pyspark.sql.types import *


def create_some_stats(db,source,target,sguid):
    sc, sqlContext = set_context(db)
    sqlContext = set_target_table(sqlContext, db, target)

    for sguid in sguids:
        desc_df = sqlContext.sql('select * from ' + source +" where sys_guid= '" + sguid +"'") #Descrptive stats (mean,min,max, etc)
        raw_data  = sqlContext.sql("select * from raw_obs_telemetry where sys_guid='" + sguid + "' ")#Raw data by for each sys_guid
        df = sqlContext.sql('select point_name,mean - 2* stddev as anomminus,mean+2*stddev as anomplus from testing where sys_guid="' + sguid + '" ')#Range for each sensor
        listtodel = sqlContext.sql('select point_name from data_quality_stats_ts where sys_guid="' + sguid +'" and bad_percentage != 100.0 ')# All sensors whose bad percentage columns isnt 100


        #Drop all sensors that have 100 bad percentage of data
        keep_list = listtodel.select("point_name").rdd.flatMap(lambda x: x).collect()

        # Filter raw_data
        raw_data = raw_data.where(raw_data.item_name.isin(keep_list))


        # Dictionary for means
        meandict = desc_df.select("point_name", "mean").rdd.collectAsMap()

        # Replace all null value
        raw_data = raw_data.rdd.map(lambda x:(x[0], x[1], x[2], x[3], (meandict[x[3]] if x[4] == None or x[4] != x[4] else x[4]), x[5], x[6], x[8], x[9],x[7]))

        # Dictionary to store range value
        rangedict = dict(df.rdd.map(lambda x:(x[0], [x[1],x[2]] )).collect())

        # filter values that are in range
        raw_data = raw_data.filter(lambda x:x[4] if x[4] >= rangedict[x[3]][0] and x[4] <= rangedict[x[3]][1] else 0)

        dataframe = raw_data.toDF()
        dataframe = dataframe.sort(dataframe._4.asc())


        print("attempting to write data from " +  sguid  + " to hive table")
        dataframe.write.insertInto('clean_raw_obs_telemetry',overwrite=False)

        
    sc.stop()

""" helper function to set context """
def set_context(db):
    sc = SparkContext(conf=SparkConf().setAll([('spark.sechduler.mode', 'FAIR'), ('spark.executor.cores', '10'), ('spark.executor.memory', '2g'), ('spark.shuffle.service.enabled', 'true'), ('spark.dynamicAllocation.enabled', 'true')]))
    sqlContext = HiveContext(sc)
    sqlContext.setConf("mapreduce.input.fileinputformat.input.dir.recursive","true")
    sqlContext.setConf("hive.input.dir.recursive","true")
    sqlContext.setConf("hive.mapred.supports.subdirectories","true")
    sqlContext.setConf("hive.supports.subdirectories","true")
    sqlContext.setConf("hive.exec.dynamic.partition", "true")
    sqlContext.setConf("hive.exec.dynamic.partition.mode", "nonstrict")

    return sc, sqlContext

def set_target_table(context, db, target):
    sqlContext = context
    sqlContext.sql("use " + db)
    return sqlContext



sguids = ["a786d94a-337c-4a94-ab25-720fe929411c", "cef46f2b-5f4d-4cad-9f37-45ebd8e6a939", "b422fcd0-564a-415c-83e8-3a41bbe21b24", "7ce751ab-18a4-43d9-9865-8d1c13be8983", "ecf0ea6e-13fe-4f10-8578-28e5b8087dd7", "839e69be-ee3b-4aca-aa22-2df933170475", "93ebaf1c-e7f0-4a65-830f-65d0aed3906f", "2d55476f-c34f-4312-b756-bff84bb45348"]
long_sguid = ["2d55476f-c34f-4312-b756-bff84bb45348"]
create_some_stats("ds01_hbt_obs_groundtruth_sbx", "testing", "clean_raw_obs_telemetry", sguids)





