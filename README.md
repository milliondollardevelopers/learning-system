## HBT Context Discovery automated model retraining package - cdt-learning-system

 The intent of this package is to implement the scripts necessary for the [end to end retraining workflow](https://confluence.honeywell.com/pages/viewpage.action?pageId=223479599).

 ![Learning System](https://confluence.honeywell.com/download/attachments/223479599/Context%20Discovery%20End%20to%20End%20flow.png?version=4&modificationDate=1528288089930&api=v2)

### Setup the environment
#### Download:
[SQL Workbench](https://dev.mysql.com/downloads/workbench/)

[Anaconda](https://www.anaconda.com/download/)

#### Create virtual environment for AI work:
```
conda env create -f environment.yml
```

#### Activate the environment:
```
source activate turing-ai
```

#### Deactivate the environment:
```
deactivate turing-ai
```

### Running the solution 

#### Create the Package
```
python setup.py sdist
```
#### Install the Package
```
pip install .
```

### Supported Functionalities:
Try out the below supported functionalities from python shell or your python app:

##### 1. Ground Truth Extraction API from Asset Registry/Model Store

API for Ground Truth Extraction
```
# Import Ground Truth Processor
from engine_data.ground_truth_processor import GroundTruthProcessor

# Select the ground truth source and create a processor object
gtp = GroundTruthProcessor(source="Fuseki")

# Load data for an array of sites
site_list = ['bcg_york','bcg_curiest','bcg_nnswhealthba','bcg_burjkhalifa','bcg_nnswhealthca','bcg_crownpodium','bcg_darwincec','bcg_emmarsouthridge',
        'bcg_etobicoke','bcg_goldenvalley','bcg_madridoffice','bcg_ladycilento','bcg_longbayforensic','bcg_longbayprison','bcg_phoenixmaryvale',
        'bcg_mlccentre','bcg_morrisplains','bcg_mpha','bcg_nnswhealthgr','bcg_nnswhealthmu','bcg_princesstower','bcg_qcc','bcg_rialto',
        'bcg_richardsonplace','bcg_rockandrollhof','bcg_stmildred','bcg_stardarling','bcg_phoenixtrevorbrowne','bcg_trillium','bcg_victoriancourts','bcg_wembley']
gtp.load_ground_truth(site_list)

# Compute Statistics
gtp.calculate_ground_truth_summary()

# Get a list of distinct sensors
gtp.distinct_sensors

# Get a list of distict class types
gtp.total_distinct_class_types

# Get instance count for each class
gtp.instances_each_class_category

# Update ground truth statistics table
gtp.update_ground_truth_stats_table()

# Update ground truth table
gtp.update_ground_truth_table()

```
##### 2. Bash Scripts to get OBS Data from blob store and create a new Hive Table with Pivoted data 
```
from engine_util.data_pivot import Transform
transformer = Transform()
transformer.pivot()
```
##### 3. Pivoted Data Extraction from Hive Table 
```
from engine_data.telemetry_data_processor import DataProcessor
dp = DataProcessor(building_name="AtlantaSoftwareCenter",data_path="training_data/")
host = 'sal04sbx.azurehdinsight.net'
database = 'db_name'
username = 'eid@sentience.local'
passkey = 'password'
port = '443'
table_name = 'table_name'
driver_version = 'Hortonworks Hive ODBC Driver'
dp.get_pivoted_data(driver_version=driver_version,username=username,database=database,host=host,passkey=passkey,port=port,table_name=table_name)
```
##### 4. Preprocessing scripts for TS data 
```
from engine_data.data_processor import DataProcessor
burj_data_processor = DataProcessor(building_name="burj_khalifa",data_path="training_data/")

# Transformation:
burj_data_processor.load_data()
burj_data_processor.get_data_head()
burj_data_processor.load_ground_truth()
burj_data_processor.merge_ground_truth()
burj_data_processor.create_label_data()

# Extract Labeled Data For specific type
burj_data_processor.labeled_training_data.loc[burj_data_processor.labeled_training_data['Model'] == "Signal Model"]


# Quality:
burj_data_processor.missing_value_stats()
burj_data_processor.clean_timestamp()
burj_data_processor.clean_col_names(unwanted_col_strings)
burj_data_processor.get_descriptive_stats()
burj_data_processor.get_memory_stats()

```
##### 5. Train Neural Network model on new Pivoted Data 
```
from engine_util.model_train import ModelTrain
ModelTrain.rnn_time_series_model_create()
```
##### 6. Model and Performance info push to blob store 
```
To be added soon!
```


### Run Tests
```
python setup.py test
```
